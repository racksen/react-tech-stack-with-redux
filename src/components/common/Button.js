import React, { Component } from "react";
import { TouchableOpacity, Text, StyleSheet } from "react-native";

const Button = ({onPress, children}) => {
  const { buttonStyle, textStyle } = styles;
  return <TouchableOpacity onPress={onPress} style={buttonStyle}>
      <Text style={textStyle}>{children}</Text>
    </TouchableOpacity>;
};


const styles = StyleSheet.create({
  textStyle: {
    alignSelf: "center",
    color: "#007fff",
    fontSize: 16,
    fontWeight: "600",
    paddingTop: 10,
    paddingBottom: 10
  },
  buttonStyle: {
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "#fff",
    borderRadius: 5,
    borderWidth: 2,
    borderColor: "#007aff",
    marginLeft: 10,
    marginRight: 10
  }
});

export {Button};
